Snippets application using the django rest framework tutorial over at http://www.django-rest-framework.org/


*Installation*
* clone this repo
* setup virtual environment
* pip install -r requirements.txt
* python manage.py createsuperuser
* python manage.py runserver